#ifndef MVPN_UTIL_TOOL_HPP
#define MVPN_UTIL_TOOL_HPP

#include <boost/noncopyable.hpp>
#include <boost/exception/error_info.hpp>
#include <fmt/core.h>
#include <utility>

#define SHARE_THIS [this,SHARE_THIS_PTR=shared_from_this()]
namespace mvpn::util
{

template<typename T, typename D>
class guard : boost::noncopyable
{
  std::decay_t<T> t_;
  D d_;
public:
  guard(std::decay_t<T> t, D d) : t_(t), d_(d)
  {};
  ~guard()
  {
    d_(t_);
  }
};

template <typename T1,typename D1>
guard<T1, D1> make_guard(T1 t, D1 d)
{
  return guard<T1, D1>(std::decay_t<T1>(t),d);
}


template <typename T>
inline void clean(T &t) noexcept
{
  static_assert(std::is_trivial_v<T>, "clean a trivial type");
  bzero(&t, sizeof(t));
}

template<typename T>
inline T make() noexcept
{
  static_assert(std::is_trivial_v<T>, "make a trivial type");
  T t{};
  clean(t);
  return t;
}

}

#endif //MVPN_UTIL_TOOL_HPP

#include "crypt.hpp"
#include <cryptopp/osrng.h>
#include <cryptopp/gcm.h>
#include <cryptopp/oids.h>
#include <boost/assert.hpp>
#include <glog/logging.h>
#include <handshake/packet.hpp>

namespace mvpn::util
{

void ECDSA::genKeyPair(mvpn::util::ECDSA::PublicKey &pub, mvpn::util::ECDSA::PrivateKey &pri)
{
  CryptoPP::AutoSeededRandomPool rng;
  pri.Initialize(rng, CryptoPP::ASN1::secp128r1());
  pri.MakePublicKey(pub);
}

void ECDSA::sign(const_buffer message, mutable_buffer signature, PrivateKey &k)
{
  using CryptoPP::byte;
  CryptoPP::AutoSeededRandomPool rng;
  Signer signer(k);
  signer.SignMessage(rng, (const byte *) message.data(), message.size(),
                     (byte *) signature.data());
}

bool ECDSA::verify(const_buffer signature, const_buffer message,const PublicKey &k)
{
  using CryptoPP::byte;
  Verifier verifier(k);
  return verifier.VerifyMessage(
    (const byte *) message.data(), message.size(),
    (const byte *) signature.data(), signature.size()
  );
}

bool ECDH::agree(
  mutable_buffer message,
  const ECDH::Public &otherPub,
  const ECDH::Private &selfPri
  )
{
  using CryptoPP::byte ;
  Domain ecdh(CryptoPP::ASN1::secp256r1());
  ecdh.AccessGroupParameters().SetPointCompression(true);
  bool valid = ecdh.Agree(
    (byte *)message.data(),
    (const byte *)selfPri.data(),
    (const byte *)otherPub.data());
  return valid;
}

void ECDH::genKeyPair(ECDH::Public &pub, ECDH::Private &pri)
{
  using CryptoPP::byte ;
  ECDH::Domain ecdh(CryptoPP::ASN1::secp256r1());
  ecdh.AccessGroupParameters().SetPointCompression(true);
  pub.resize(ecdh.PublicKeyLength());
  pri.resize(ecdh.PrivateKeyLength());
  CryptoPP::AutoSeededRandomPool rng;
  ecdh.GenerateKeyPair(rng,(byte *)pri.data(),(byte *)pub.data()) ;
}

void sha256(const_buffer message, mutable_buffer digest)
{
  using CryptoPP::byte;
  BOOST_ASSERT(digest.size() >= CryptoPP::SHA256::DIGESTSIZE);
  using CryptoPP::SHA256;
  SHA256 sha256;
  sha256.CalculateDigest((byte *) digest.data(), (const byte *) message.data(),
                         message.size());
}


void aes_gcm_enc(
  const_buffer key,
  const_buffer nonce,
  const_buffer aData,
  const_buffer pData,
  mutable_buffer eData)
{
  
  using namespace CryptoPP;
  GCM<AES>::Encryption e;
  e.SetKeyWithIV(
    (const byte *)key.data(),key.size(),
    (const byte *)nonce.data(),nonce.size());
  AuthenticatedEncryptionFilter ef(
    e,
    new ArraySink((byte *) eData.data(), eData.size()),
    false,
    packet::MAC_LEN);
  ef.ChannelPut("AAD",(byte *)aData.data(),aData.size());
  ef.ChannelMessageEnd("AAD");
  ef.ChannelPut("",(byte *)pData.data(),pData.size());
  ef.ChannelMessageEnd("");
}

bool aes_gcm_dec(
  const_buffer key,
  const_buffer nonce,
  const_buffer aData,
  const_buffer eData,
  mutable_buffer pData)
{
  using namespace CryptoPP;
  GCM<AES>::Decryption d;
  d.SetKeyWithIV((const byte *)key.data(),key.size(),(const byte *)nonce.data(),nonce.size());
  try
  {
    AuthenticatedDecryptionFilter df(
      d, nullptr,
      AuthenticatedDecryptionFilter::MAC_AT_END| AuthenticatedDecryptionFilter::THROW_EXCEPTION,
      packet::MAC_LEN
    );
    df.ChannelPut("AAD", (byte *) aData.data(), aData.size());
    df.ChannelPut("", (byte *) eData.data(), eData.size());
    df.ChannelMessageEnd("AAD");
    df.ChannelMessageEnd("");
    bool b = df.GetLastResult();
    if ( !b )
      return false;
    df.SetRetrievalChannel("");
    df.Get((byte *) pData.data(), pData.size());
  }
  catch ( CryptoPP::Exception &e )
  {
    LOG(ERROR) << e.what();
    return false;
  }
  
  return true;
}

void genRandom(mutable_buffer random)
{
  CryptoPP::AutoSeededRandomPool rng;
  rng.GenerateBlock((CryptoPP::byte *)random.data(),random.size());
}


}
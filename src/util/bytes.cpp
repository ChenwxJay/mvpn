#include "bytes.hpp"
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
namespace mvpn::util
{

bytes::bytes(size_t alloc_sz) :
  storage_(new unsigned char[alloc_sz]()),
  storage_begin_(storage_),
  storage_end_(storage_begin_ + alloc_sz),
  begin_(storage_begin_),
  end_(begin_)
{
}

bytes::bytes(const bytes &other) :
  storage_(new unsigned char[other.capacity()]()),
  storage_begin_(storage_),
  storage_end_(storage_+other.capacity()),
  begin_(storage_begin_),
  end_(begin_ + other.size())
{
  std::copy(other.begin(),other.end(),begin_);
}

bytes::bytes(bytes &&other) noexcept :
  storage_(std::exchange(other.storage_, nullptr)),
  storage_begin_(std::exchange(other.storage_begin_, nullptr)),
  storage_end_(std::exchange(other.storage_end_, nullptr)),
  begin_(std::exchange(other.begin_,nullptr)),
  end_(std::exchange(other.end_,nullptr))
{
}

bytes::~bytes()
{
  delete [] storage_;
}

bytes &bytes::operator=(bytes other) noexcept
{
  swap(other);
  return *this;
}

bytes::iterator bytes::begin() noexcept
{
  return begin_;
}

bytes::iterator bytes::end() noexcept
{
  return end_;
}

bytes::const_iterator bytes::begin() const noexcept
{
  return begin_;
}

bytes::const_iterator bytes::end() const noexcept
{
  return end_;
}

std::size_t bytes::size() const noexcept
{
  return end_ - begin_;
}

std::size_t bytes::capacity() const noexcept
{
  return storage_end_ - storage_begin_;
}

std::size_t bytes::remain_capacity() const noexcept
{
  return storage_end_ - end_;
}

void bytes::resize(std::size_t n)
{
  adjust(n);
  end_ = begin_ + n;
}

void bytes::reserve(std::size_t n)
{
  if ( n < capacity())
    return;
  adjust(n);
}

const_buffer bytes::view(bytes::iterator start, bytes::iterator stop) const
{
  auto[data, len] = get_view(start, stop - start);
  return const_buffer(data, len);
}

const_buffer bytes::view(std::size_t start, std::size_t stop) const
{
  auto[data, len] = get_view(begin_ + start,stop-start);
  return const_buffer(data, len);
}

mutable_buffer bytes::prepare(std::size_t n)
{
  if ( n > remain_capacity())
  {
    adjust((n + capacity()) * 2);
  }
  auto[data, len] = get_view(end_, n);
  return mutable_buffer(data, len);
}

void bytes::commit(std::size_t n)
{
  end_ += n;
}

void bytes::consume(std::size_t sz)
{
  begin_ += sz;
}

void bytes::align()
{
  if ( storage_begin_ == begin_ )
    return;
  std::move(begin_, end_, storage_begin_);
  end_ -= (begin_ - storage_begin_);
  begin_ = storage_begin_;
}

void bytes::clear()
{
  begin_ = end_ = storage_begin_;
}

void bytes::swap(bytes &other) noexcept
{
  std::swap(other.storage_, storage_);
  std::swap(other.storage_begin_, storage_begin_);
  std::swap(other.begin_, begin_);
  std::swap(other.end_, end_);
  std::swap(other.storage_end_, storage_end_);
}

void bytes::adjust(std::size_t n)
{
  if ( n == capacity())
    return;
  bytes other(n);
  auto o = other.prepare(std::min(n,size()));
  std::copy(begin_,std::min(begin_+n,end_),(unsigned char *)o.data());
  other.commit(o.size());
  swap(other);
}

std::pair<unsigned char *, std::size_t>
bytes::get_view(bytes::iterator begin, std::size_t pos) const
{
  return std::make_pair(begin, pos);
}

std::string bytes::debug_string(const std::initializer_list<std::size_t> &sep)
{
  using namespace CryptoPP;
  std::string hex_str;
  iterator current = begin_;
  for (auto i : sep)
  {
    if (current + i > end_)
    {
      hex_str+="|OF";
      break;
    }
    std::string tmp;
    ArraySource tAS0(current,i,true,new HexEncoder(new StringSink(tmp)));
    current+=i;
    hex_str += tmp+"|";
    if (current >= end_)
      break;
  }
  return hex_str;
}


}
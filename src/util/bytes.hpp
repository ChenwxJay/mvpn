//
// Created by maxtorm on 18-10-22.
//

#ifndef MVPN_UTIL_BYTES_HPP
#define MVPN_UTIL_BYTES_HPP

#include <cstdint>
#include <memory>
#include <boost/asio/buffer.hpp>

using namespace boost::asio;

namespace mvpn::util
{
class bytes
{
public:
  using iterator = unsigned char *;
  using const_iterator = const unsigned char *;
private:
  unsigned char *storage_;
  iterator storage_begin_;
  iterator storage_end_;
  iterator begin_;
  iterator end_;
public:
  explicit bytes(std::size_t alloc_sz = 0);
  
  bytes(const bytes &other);
  
  bytes(bytes &&other) noexcept;
  
  ~bytes();
  
  bytes &operator=(bytes other) noexcept;
  
  iterator begin() noexcept;
  
  iterator end() noexcept;
  
  const_iterator begin() const noexcept;
  
  const_iterator end() const noexcept;
  
  std::size_t size() const noexcept;
  
  std::size_t capacity() const noexcept;
  
  std::size_t remain_capacity() const noexcept;
  
  void resize(std::size_t n);
  
  void reserve(std::size_t n);
  
  const_buffer view(iterator start, iterator stop) const;
  
  const_buffer view(std::size_t start, std::size_t stop) const;
  
  template<typename T>
  void view(T & t,std::size_t pos = 0) const
  {
    static_assert(std::is_trivial_v<T>,"view a trivial type");
    auto v = get_view(begin_+pos, sizeof(T));
    memcpy(&t,v.first, sizeof(t));
  }
  
  mutable_buffer prepare(std::size_t n);
  
  void commit(std::size_t n);
  
  void consume(std::size_t sz);
  
  void align();
  
  void clear();
  
  void swap(bytes &other) noexcept;
  
  
  std::string debug_string(const std::initializer_list<std::size_t> &sep);

private:
  void adjust(std::size_t n);
  std::pair<unsigned char *, std::size_t>
  get_view(iterator begin, std::size_t pos) const;
};

}
#endif //MVPN_UTIL_BYTES_HPP

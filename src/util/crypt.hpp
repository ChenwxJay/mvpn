//
// Created by maxtorm on 18-10-23.
//

#ifndef MVPN_CRYPT_HPP
#define MVPN_CRYPT_HPP

#include <cryptopp/eccrypto.h>
#include <boost/asio/buffer.hpp>
namespace mvpn::util
{
using boost::asio::const_buffer;
using boost::asio::mutable_buffer;

class ECDSA
{
  using impl_type = CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>;
public:
  using Signer = impl_type::Signer;
  using Verifier = impl_type::Verifier;
  using PrivateKey = impl_type::PrivateKey;
  using PublicKey = impl_type::PublicKey;
  
  static void genKeyPair(PublicKey &, PrivateKey &);
  
  static void sign(
    const_buffer message,
    mutable_buffer signature,
    ECDSA::PrivateKey &k);
  
  static bool verify(
    const_buffer signature,
    const_buffer message,
    const ECDSA::PublicKey &k);
  
};

class ECDH
{
  using impl_type = CryptoPP::ECDH<CryptoPP::ECP>;
public:
  using Domain = impl_type::Domain;
  using Private = std::string;
  using Public = std::string;
  static bool agree(
    mutable_buffer message,
    const Public & otherPub,
    const Private &selfPri);
  static void genKeyPair(Public &pub,Private &pri);
};

void genRandom(mutable_buffer random);

void sha256(const_buffer message, mutable_buffer digest);

void aes_gcm_enc(
  const_buffer key,
  const_buffer nonce,
  const_buffer aData,
  const_buffer pData,
  mutable_buffer eData
);

bool aes_gcm_dec(
  const_buffer key,
  const_buffer nonce,
  const_buffer aData,
  const_buffer eData,
  mutable_buffer pData
);
}
#endif //MVPN_CRYPT_HPP

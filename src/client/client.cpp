#include "client.hpp"
#include <connection/forward.hpp>

using namespace boost;
namespace mvpn
{

client::client(
  client::config cfg,
  boost::asio::io_context &ioctx,
  device::tun &tun) :
  cfg_(std::move(cfg)),
  ioctx_(ioctx),
  tun_(tun),
  net_(ioctx_)
{
}

void client::start()
{
  asio::ip::tcp::endpoint peer_ep(cfg_.remote.address, cfg_.remote.port);
  LOG(INFO) <<"connect to: "<<peer_ep;
  net_.async_connect(
    peer_ep,
    [this](system::error_code ec)
    {
      service::key_exchange::attribute attr;
      {
        attr.identify = cfg_.identify;
        attr.privateKey = cfg_.privateKey;
        attr.peerKeys
            .insert_or_assign(cfg_.remote.identify, cfg_.remote.publicKey);
      }
      auto service_key_exchange =
        service::make_key_exchange(std::move(net_), std::move(attr),
                                   service::key_exchange::type::CLIENT, 1000);
      service_key_exchange->async_exchange(
        [this](
          service::key_exchange::error_code ec,
          service::key_exchange::peer_attribute pAttr,
          asio::ip::tcp::socket ps)
        {
          net_ = std::move(ps);
          if ( ec.code != service::key_exchange::error_value::ERRSUCCESS )
          {
            LOG(ERROR) << ec.what;
            return;
          }
          auto forward = service::make_forward(tun_, std::move(net_),
                                               pAttr.key);
          forward->start(
            [](service::forward::error_code ec, boost::asio::ip::tcp::socket s)
            {
              LOG(ERROR) << ec.code.message();
            });
        }
        );
    });
}

}

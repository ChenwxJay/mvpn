//
// Created by maxtorm on 18-11-5.
//

#include "client.hpp"
namespace mvpn
{
void client::config::from_property_tree(const boost::property_tree::ptree &pt)
{
  identify.resize(32);
  util::sha256(
    boost::asio::buffer(pt.get<std::string>("Identify")),
    boost::asio::buffer(identify));
  auto cPriB64 = pt.get<std::string>("PrivateKey");
  std::string cPri;
  CryptoPP::StringSource tSS0(
    cPriB64, true,
    new CryptoPP::Base64Decoder(new CryptoPP::StringSink(cPri)));
  privateKey.Load(CryptoPP::StringSource(cPri, true).Ref());
  auto rmt = pt.get_child("Remote");
  auto rPubB64 = rmt.get<std::string>("PublicKey");
  std::string rPub;
  CryptoPP::StringSource tSS1(
    rPubB64, true,
    new CryptoPP::Base64Decoder(new CryptoPP::StringSink(rPub)));
  remote.publicKey.Load(CryptoPP::StringSource(rPub, true).Ref());
  remote.address = boost::asio::ip::make_address(
    rmt.get<std::string>("Address"));
  remote.port = rmt.get<uint16_t>("Port");
  remote.identify.resize(32);
  util::sha256(
    boost::asio::buffer(rmt.get<std::string>("Identify")),
    boost::asio::buffer(remote.identify)
  );
}

client::config::config(const boost::property_tree::ptree &pt)
{
  from_property_tree(pt);
}
}
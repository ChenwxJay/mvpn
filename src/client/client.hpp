//
// Created by maxtorm on 18-11-5.
//

#ifndef MVPN_CLIENT_HPP
#define MVPN_CLIENT_HPP

#include <string>

#include <util/crypt.hpp>
#include <util/bytes.hpp>
#include <device/tun.hpp>
#include <connection/key_exchange.hpp>

#include <boost/asio/ip/address.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/system_timer.hpp>

#include <cryptopp/filters.h>
#include <cryptopp/base64.h>

namespace mvpn
{
class client
{
  class forward_service;
  using shared_forward_service = std::shared_ptr<forward_service>;
  
public:
  struct config
  {
    struct remote_s
    {
      util::ECDSA::PublicKey publicKey;
      boost::asio::ip::address address;
      uint16_t port;
      std::string identify;
    };
    remote_s remote;
    std::string identify;
    util::ECDSA::PrivateKey privateKey;
    
    config() = default;
    
    explicit config(const boost::property_tree::ptree &pt);
    
    void from_property_tree(const boost::property_tree::ptree &pt);
  };
  
  client(
    config cfg,
    boost::asio::io_context &ioctx,
    device::tun &tun
    );
  
  void start();

private:
  config cfg_;
  boost::asio::io_context &ioctx_;
  device::tun &tun_;
  boost::asio::ip::tcp::socket net_;
};
}


#endif //MVPN_CLIENT_HPP

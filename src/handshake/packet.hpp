//
// Created by maxtorm on 18-10-21.
//

#ifndef MVPN_HANDSHAKE_PACKET_HPP
#define MVPN_HANDSHAKE_PACKET_HPP

#include <cstdint>
#define MVPN_BIG_ENDIAN
#define MVPN_LITTLE_ENDIAN
#define MVPN_SEQUENTIAL_DATA
#define MVPN_PACKED_STRUCT struct __attribute__((packed))

namespace mvpn::packet
{
constexpr std::size_t MSG_MAX            =               (2048);
constexpr uint8_t  OPT_PSH               =               (1<<1);
constexpr uint8_t  OPT_CMP               =               (1<<2);
constexpr uint8_t  OPT_KEEP               =              (1<<3);
constexpr std::size_t SIGNATURE_LEN      =               (32);
constexpr std::size_t PUBLIC_KEY_LEN     =               (33);
constexpr std::size_t IDENTIFY_LEN       =               (32);
constexpr std::size_t ADDRESS_MAX        =               (16);
constexpr std::size_t NONCE_LEN          =               (32);
constexpr std::size_t MAC_LEN            =               (16);
constexpr std::size_t KEY_DIGEST_LEN     =               (32);
constexpr std::size_t ADDRESS_V4         =               (4);
constexpr std::size_t ADDRESS_V6         =               (6);

using byte = uint8_t;

MVPN_PACKED_STRUCT Header
{
  MVPN_BIG_ENDIAN                     uint8_t type;
  MVPN_BIG_ENDIAN                     uint16_t length;
};
using packet_type = decltype(Header::type);
using length_type = decltype(Header::length);
constexpr packet_type TYP_KEYEXCHANGE(1);
constexpr packet_type TYP_KEYCONFIRM(2);
constexpr packet_type TYP_MESSAGE(3);

MVPN_PACKED_STRUCT KeyExchange
{
  MVPN_SEQUENTIAL_DATA                byte identify[IDENTIFY_LEN];
  MVPN_SEQUENTIAL_DATA                byte publicKey[PUBLIC_KEY_LEN];
  MVPN_SEQUENTIAL_DATA                byte signature[SIGNATURE_LEN];
};

MVPN_PACKED_STRUCT KeyExchangeSign
{
  MVPN_SEQUENTIAL_DATA                byte identify[IDENTIFY_LEN];
  MVPN_SEQUENTIAL_DATA                byte publicKey[PUBLIC_KEY_LEN];
};

MVPN_PACKED_STRUCT KeyConfirm
{
  MVPN_SEQUENTIAL_DATA                byte digest[KEY_DIGEST_LEN];
};

MVPN_PACKED_STRUCT ClientConfig
{
  MVPN_SEQUENTIAL_DATA                byte nonce[NONCE_LEN];
  MVPN_SEQUENTIAL_DATA                byte mac[MAC_LEN];
  MVPN_BIG_ENDIAN                     byte address_type;
  MVPN_SEQUENTIAL_DATA                byte address[ADDRESS_MAX];
};

MVPN_PACKED_STRUCT ServerConfig
{
  MVPN_SEQUENTIAL_DATA                byte nonce[NONCE_LEN];
  MVPN_SEQUENTIAL_DATA                byte mac[MAC_LEN];
  MVPN_BIG_ENDIAN                     uint32_t result;
};

MVPN_PACKED_STRUCT Message
{
  MVPN_SEQUENTIAL_DATA                byte nonce[NONCE_LEN];
  MVPN_BIG_ENDIAN                     uint8_t option;
};


}
#endif //MVPN_PACKET_HPP

#include "server.hpp"
using namespace boost;
using namespace asio;
namespace mvpn
{

server::server(
  server::config cfg,
  boost::asio::io_context &ioctx,
  device::tun &tun) :
  cfg_(std::move(cfg)),
  ioctx_(ioctx),
  tun_(tun),
  acceptor_(ioctx_)
{
  ip::tcp::endpoint ep(cfg_.address,cfg_.port);
  acceptor_.open(ep.protocol());
  acceptor_.set_option(ip::tcp::no_delay(true));
  acceptor_.bind(ep);
  acceptor_.listen();
}

void server::start()
{
  acceptor_.async_accept(
    [this](system::error_code ec,ip::tcp::socket s)
    {
      LOG(INFO) << s.remote_endpoint()<<" connected";
      session_attribute attr(cfg_,std::move(s),tun_);
      auto sess = make_session(std::move(attr));
      sess->start();
      start();
    });
}
}

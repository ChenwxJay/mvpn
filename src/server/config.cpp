//
// Created by maxtorm on 18-11-5.
//
#include "server.hpp"
#include <boost/foreach.hpp>
namespace mvpn
{
void server::config::from_property_tree(const boost::property_tree::ptree &pt)
{
  address = boost::asio::ip::make_address(pt.get<std::string>("Address"));
  port = pt.get<uint16_t>("Port");
  identify.resize(32);
  util::sha256(
    boost::asio::buffer(pt.get<std::string>("Identify")),
    boost::asio::buffer(identify));
  auto lPriB64 = pt.get<std::string>("PrivateKey");
  std::string lPri;
  CryptoPP::StringSource tSS0(
    lPriB64, true,
    new CryptoPP::Base64Decoder(new CryptoPP::StringSink(lPri)));
  privateKey.Load(CryptoPP::StringSource(lPri, true).Ref());
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v,pt.get_child("Clients"))
  {
    auto client = v.second;
    std::string identify(32, '\0');
    util::sha256(
      boost::asio::buffer(client.get<std::string>("Identify")),
      boost::asio::buffer(identify)
    );
    util::ECDSA::PublicKey publicKey;
    auto cPubB64 = client.get<std::string>("PublicKey");
    std::string cPub;
    CryptoPP::StringSource tSS1(
      cPubB64, true,
      new CryptoPP::Base64Decoder(new CryptoPP::StringSink(cPub)));
    publicKey.Load(CryptoPP::StringSource(cPub, true).Ref());
    clients.insert(
      std::make_pair(std::move(identify), std::move(publicKey)));
  }
}

server::config::config(const boost::property_tree::ptree &pt)
{
  from_property_tree(pt);
}

}
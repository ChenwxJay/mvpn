#ifndef MVPN_SERVER_HPP
#define MVPN_SERVER_HPP

#include <map>
#include <util/crypt.hpp>
#include <util/bytes.hpp>
#include <device/tun.hpp>

#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/asio/io_context.hpp>

#include <cryptopp/base64.h>
#include <cryptopp/filters.h>

namespace mvpn
{
class server
{
private:

public:
  struct config
  {
    boost::asio::ip::address address;
    uint16_t port{};
    std::string identify;
    util::ECDSA::PrivateKey privateKey;
    std::map<std::string, util::ECDSA::PublicKey> clients;
    
    config() = default;
    
    explicit config(const boost::property_tree::ptree &pt);
    
    void from_property_tree(const boost::property_tree::ptree &pt);
  };
  
  server(
    config cfg,
    boost::asio::io_context &ioctx,
    device::tun &tun
  );
  
  void start();

private:
  struct session_attribute
  {
    config &cfg;
    boost::asio::ip::tcp::socket net;
    device::tun &tun;
    session_attribute(
      config &cfg_,
      boost::asio::ip::tcp::socket net_,
      device::tun &tun_) :
      cfg(cfg_),
      net(std::move(net_)),
      tun(tun_)
    {}
  };
  
  config cfg_;
  boost::asio::io_context &ioctx_;
  device::tun &tun_;
  boost::asio::ip::tcp::acceptor acceptor_;
  
  class session;
  
  using shared_session = std::shared_ptr<session>;
  
  static shared_session make_session(session_attribute attr);
  
  class session :
    public boost::noncopyable,
    public std::enable_shared_from_this<session>
  {
    struct net
    {
      boost::asio::ip::tcp::socket io;
      util::bytes r;
      util::bytes w;
      
      explicit net(boost::asio::ip::tcp::socket io_) : io(std::move(io_))
      {};
    };
    struct tun
    {
      device::tun &io;
      util::bytes r;
      util::bytes w;
      
      explicit tun(device::tun &io_) : io(io_)
      {};
    };
  public:
    void start();
  private:
    config &cfg_;
    std::string key_;
    net net_;
    tun tun_;
    session(session_attribute attr);
    friend shared_session server::make_session(session_attribute attr);
  };
};


}


#endif //MVPN_SERVER_HPP

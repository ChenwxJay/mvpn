//
// Created by maxtorm on 18-11-7.
//

#ifndef MVPN_FORWARD_SERVICE_HPP
#define MVPN_FORWARD_SERVICE_HPP

#include <memory>
#include <boost/noncopyable.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <device/tun.hpp>
#include <util/bytes.hpp>
#include "forward_fwd.hpp"
namespace mvpn::service
{

class forward :
  public boost::noncopyable,
  public std::enable_shared_from_this<forward>
{
  struct tun
  {
    device::tun & io;
    util::bytes r;
    util::bytes w;
    explicit tun(device::tun & io_) : io(io_) {}
  };
  struct net
  {
    boost::asio::ip::tcp::socket io;
    util::bytes r;
    util::bytes w;
    explicit net(boost::asio::ip::tcp::socket io_) : io(std::move(io_)) {}
  };
public:
  enum class error_source
  {
    NONE,
    TUN,
    NET
  };
  struct error_code
  {
    error_source source = error_source::NONE;
    boost::system::error_code code;
  };
  using callback_type = std::function<
    void(error_code,boost::asio::ip::tcp::socket)>;
  void start(callback_type && cb);
private:
  void tun_to_net();
  void build_to_net_packet();
  void to_net();
  void net_to_tun();
  bool parse_packet();
  void to_tun();
  void invoke(error_code ec);
  bool EC_CHK(error_source src,boost::system::error_code &ec);
  tun tun_;
  net net_;
  std::string key_;
  callback_type cb_;
  forward(device::tun & t,boost::asio::ip::tcp::socket s,std::string key);
  friend shared_forward make_forward(
    device::tun & t,boost::asio::ip::tcp::socket s,std::string key
    );
};

shared_forward make_forward(
  device::tun & t,boost::asio::ip::tcp::socket s,std::string key
);
}


#endif //MVPN_FORWARD_SERVICE_HPP

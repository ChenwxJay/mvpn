//
// Created by maxtorm on 18-11-7.
//

#include "forward.hpp"
#include <handshake/packet.hpp>
#include <boost/endian/conversion.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <util/crypt.hpp>
#include <glog/logging.h>
using namespace boost;
using namespace asio;
using namespace endian;
using namespace mvpn::packet;
namespace mvpn::service
{

forward::forward(
  device::tun &t,
  ip::tcp::socket s,
  std::string key) :
  tun_(t),
  net_(std::move(s)),
  key_(std::move(key))
{}

shared_forward make_forward(device::tun &t, ip::tcp::socket s, std::string key)
{
  return shared_forward(new forward(t, std::move(s), std::move(key)));
}

void forward::start(forward::callback_type &&cb)
{
  cb_ = std::move(cb);
  tun_to_net();
  net_to_tun();
}

void forward::tun_to_net()
{
  tun_.r.clear();
  auto rbf = tun_.r.prepare(MSG_MAX);
  tun_.io.async_read_some(
    rbf,
    SHARE_THIS(system::error_code ec, std::size_t nr)
    {
      if ( EC_CHK(error_source::TUN, ec)) return;
      LOG(INFO) << "read: " << nr << " from tun";
      tun_.r.commit(nr);
      build_to_net_packet();
      to_net();
    }
  );
}

void forward::build_to_net_packet()
{
  net_.w.clear();
  const auto payloadSz = tun_.r.size();
  net_.w.reserve(sizeof(Header) + sizeof(Message) + payloadSz + MAC_LEN);
  
  auto header = (Header *) net_.w.prepare(sizeof(Header)).data();
  {
    header->type = native_to_big(packet_type(TYP_MESSAGE));
    header->length = native_to_big(
      length_type(sizeof(Message) + payloadSz + MAC_LEN));
    net_.w.commit(sizeof(Header));
  }
  
  auto message = (Message *) net_.w.prepare(sizeof(Message)).data();
  {
    message->option = native_to_big(packet_type(OPT_PSH));
    util::genRandom(buffer(message->nonce));
    net_.w.commit(sizeof(Message));
    
    util::aes_gcm_enc(
      buffer(key_),
      buffer(message->nonce),
      buffer(message, sizeof(Message)),
      tun_.r.view(0, payloadSz),
      net_.w.prepare(payloadSz + MAC_LEN)
    );
    net_.w.commit(payloadSz + MAC_LEN);
    tun_.r.clear();
  }
}

void forward::to_net()
{
  async_write(
    net_.io, net_.w.view(0, net_.w.size()),
    SHARE_THIS(system::error_code ec, std::size_t nw)
    {
      if ( EC_CHK(error_source::NET, ec)) return;
      LOG(INFO) << "write: " << nw << " to net";
      net_.w.clear();
      tun_to_net();
    }
  );
}

void forward::to_tun()
{
  tun_.io.async_write_some(
    tun_.w.view(0, tun_.w.size()),
    SHARE_THIS(system::error_code ec, std::size_t nw)
    {
      if ( EC_CHK(error_source::TUN, ec)) return;
      LOG(INFO) << "write: " << nw << " to tun";
      tun_.w.clear();
      net_to_tun();
    }
  );
}

void forward::net_to_tun()
{
  net_.r.clear();
  async_read(
    net_.io, net_.r.prepare(sizeof(Header)),
    SHARE_THIS(system::error_code ec, std::size_t nr)
    {
      if ( EC_CHK(error_source::NET, ec)) return;
      LOG(INFO) << "read: " << nr << " from net";
      net_.r.commit(nr);
      auto header = util::make<Header>();
      {
        net_.r.view(header);
        header.length = big_to_native(header.length);
        header.type = big_to_native(header.type);
      }
      net_.r.clear();
      //TODO
      async_read(
        net_.io, net_.r.prepare(header.length),
        SHARE_THIS(system::error_code ec, std::size_t nr)
        {
          if ( EC_CHK(error_source::NET, ec)) return;
          LOG(INFO) << "read: " << nr << " from net";
          net_.r.commit(nr);
          if ( parse_packet())
          {
            LOG(WARNING) << "error packet from net";
            net_to_tun();
            return;
          }
          to_tun();
        }
      );
    }
  );
}

bool forward::parse_packet()
{
  tun_.w.clear();
  auto message = (Message *) net_.r.view(0, sizeof(Message)).data();
  auto ePayload = net_.r.view(sizeof(Message),net_.r.size());
  auto pPayload = tun_.w.prepare(ePayload.size()-MAC_LEN);
  bool good = util::aes_gcm_dec(
    buffer(key_),
    buffer(message->nonce),
    buffer( message, sizeof(Message)),
    ePayload,
    pPayload
  );
  if ( !good )
  {
    tun_.w.clear();
    return true;
  }
  tun_.w.commit(pPayload.size());
  return false;
}

void forward::invoke(error_code ec)
{
  cb_(std::move(ec), std::move(net_.io));
}

bool forward::EC_CHK(
  error_source src,
  system::error_code &ec)
{
  if ( ec )
  {
    invoke(error_code{src, ec});
    return true;
  }
  return false;
}
}
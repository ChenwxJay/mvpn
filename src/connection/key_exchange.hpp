#ifndef MVPN_CONNECTION_SERVICE_KEY_EXCHANGE_SERVICE_HPP
#define MVPN_CONNECTION_SERVICE_KEY_EXCHANGE_SERVICE_HPP

#include "key_exchange_fwd.hpp"

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/system_timer.hpp>

#include <util/crypt.hpp>
#include <util/bytes.hpp>

#include <glog/logging.h>

#include <memory>

namespace mvpn::service
{
class key_exchange :
  public boost::noncopyable,
  public std::enable_shared_from_this<key_exchange>
{
public:
  enum class type
  {
    SERVER = 0,
    CLIENT = 1
  };
  
  struct attribute
  {
    std::string identify;
    util::ECDSA::PrivateKey privateKey;
    std::map<std::string,util::ECDSA::PublicKey> peerKeys;
  };
  
  struct peer_attribute
  {
    std::string identify;
    util::ECDSA::PublicKey publicKey;
    std::string key;
  };
  
  enum error_value
  {
    ERRSUCCESS = 0,
    ERRINVSIG = 1,
    ERRFAILAGREE = 2,
    ERRTIMEOUT = 3,
    ERRSOCKET = 4,
    ERRPEERINVKEY = 5,
    ERRNOPEER = 6,
    ERRINVPKT = 7
  };
  
  struct error_code
  {
    error_value code;
    std::string what;
  };
  
  using callback_type = std::function<
    void(error_code,peer_attribute,boost::asio::ip::tcp::socket)>;
  void async_exchange(callback_type &&cb);
  
private:
  boost::asio::ip::tcp::socket peer_;
  attribute attr_;
  peer_attribute pAttr_;
  type type_;
  int rttTimeout_;
  boost::asio::system_timer timer_;
  
  util::ECDH::Private lECDHPri_;
  util::ECDH::Public lECDHPub_;
  
  
  util::bytes r_;
  util::bytes w_;
  
  callback_type cb_;
  
  key_exchange(
    boost::asio::ip::tcp::socket ps,
    attribute attr,
    type type,
    int rttTimeout
  );
  
  friend shared_key_exchange make_key_exchange(
    boost::asio::ip::tcp::socket p,
    attribute attr,
    type typ,
    int rttTimeout
  );
  
  bool EC_CHK(const boost::system::error_code &ec);
  
  void build_exchange();
  void receive_exchange();
  void send_exchange();
  bool validate_exchange();
  void receive_confirm();
  void send_confirm();
  void timer_start(int timeout);
  void timer_stop();
  void invokeError(error_value code,std::string what);
  void invokeSuccess(peer_attribute pAttr);
};

shared_key_exchange make_key_exchange(
  boost::asio::ip::tcp::socket ps,
  key_exchange::attribute attr,
  key_exchange::type typ,
  int rttTimeout
);

}


#endif //MVPN_CONNECTION_SERVICE_ECDH_KEY_EXCHANGE_HPP

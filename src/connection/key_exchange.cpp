#include <utility>

#include "key_exchange.hpp"
#include <boost/asio/read.hpp>
#include <connection/def.hpp>
#include <handshake/packet.hpp>
#include <boost/endian/conversion.hpp>
#include <util/tool.hpp>
#include <boost/asio/write.hpp>
#include <cryptopp/base64.h>

using namespace boost;
using namespace asio;
using namespace endian;
namespace mvpn::service
{

shared_key_exchange make_key_exchange(
  asio::ip::tcp::socket ps,
  key_exchange::attribute attr,
  key_exchange::type typ,
  int rttTimeout
)
{
  return shared_key_exchange(
    new key_exchange(
      std::move(ps),
      std::move(attr),
      typ,
      rttTimeout
    )
  );
}

key_exchange::key_exchange(
  boost::asio::ip::tcp::socket p,
  key_exchange::attribute attr,
  key_exchange::type type,
  int rttTimeout) :
  peer_(std::move(p)),
  attr_(std::move(attr)),
  type_(type),
  rttTimeout_(rttTimeout),
  timer_(peer_.get_executor().context())
{
  util::ECDH().genKeyPair(lECDHPub_, lECDHPri_);
}

void key_exchange::async_exchange(callback_type &&cb)
{
  cb_ = std::move(cb);
  switch ( type_ )
  {
    case type::CLIENT:
      LOG(INFO) << "async_exchange as client";
      send_exchange();
      break;
    case type::SERVER:
      LOG(INFO) << "async_exchange as server";
      receive_exchange();
      break;
  }
}

bool key_exchange::EC_CHK(const system::error_code &ec)
{
  if (ec)
  {
    if ( ec == asio::error::operation_aborted)
    {
      invokeError(ERRTIMEOUT, "connection timeout");
    }
    else
    {
      invokeError(ERRSOCKET, ec.message());
    }
    return true;
  }
  return false;
}

void key_exchange::timer_start(int timeout)
{
  timer_.expires_after(std::chrono::milliseconds(timeout));
  timer_.async_wait(
    SHARE_THIS(system::error_code ec)
    {
      if ( ec )
      {
        if ( ec == asio::error::operation_aborted )
          return;
        throw boost::system::system_error(ec);
      }
      peer_.cancel();
    });
}

void key_exchange::timer_stop()
{
  timer_.cancel();
}

void key_exchange::invokeError(error_value code, std::string what)
{
  cb_(error_code{code, std::move(what)}, peer_attribute{}, std::move(peer_));
}

void key_exchange::invokeSuccess(peer_attribute pAttr)
{
  cb_(error_code{ERRSUCCESS, ""}, std::move(pAttr), std::move(peer_));
}

void key_exchange::build_exchange()
{
  using namespace packet;
  w_.clear();
  {
    auto header = (Header *) w_.prepare(sizeof(Header)).data();
    header->type = native_to_big((packet_type(TYP_KEYEXCHANGE)));
    header->length = native_to_big((length_type) (sizeof(KeyExchange)));
    w_.commit(sizeof(Header));
  }
  {
    auto exchange = (KeyExchange *) w_.prepare(sizeof(KeyExchange)).data();
    memcpy(exchange->identify, attr_.identify.data(), IDENTIFY_LEN);
    memcpy(exchange->publicKey, lECDHPub_.data(), PUBLIC_KEY_LEN);
    util::ECDSA ecdsa;
    ecdsa.sign(
      buffer((KeyExchangeSign *) exchange, sizeof(KeyExchangeSign)),
      buffer(exchange->signature),
      attr_.privateKey
    );
    w_.commit(sizeof(KeyExchange));
  }
}

void key_exchange::receive_exchange()
{
  using namespace packet;
  r_.clear();
  auto headerBf = r_.prepare(sizeof(packet::Header));
  timer_start(rttTimeout_);
  asio::async_read(
    peer_, headerBf,
    SHARE_THIS(system::error_code ec, std::size_t nr)
    {
      timer_stop();
      if ( EC_CHK(ec))return;
      r_.commit(nr);
      auto header = util::make<Header>();
      r_.view(header);
      header.type=big_to_native(header.type);
      header.length=big_to_native(header.length);
      if ( header.type != TYP_KEYEXCHANGE )
      {
        invokeError(ERRINVPKT, fmt::format("invalid type: {}", header.type));
        return;
      }
      if ( header.length != sizeof(KeyExchange))
      {
        invokeError(ERRINVPKT,
                    fmt::format("invalid length: {}", header.length));
        return;
      }
      auto exchangeBf = r_.prepare(header.length);
      timer_start(rttTimeout_);
      asio::async_read(
        peer_, exchangeBf,
        SHARE_THIS(system::error_code ec, std::size_t nr)
        {
          timer_stop();
          if ( EC_CHK(ec)) return;
          r_.commit(nr);
          if ( validate_exchange()) return;
          switch ( type_ )
          {
            case type::SERVER:
              send_exchange();
              break;
            case type::CLIENT:
              send_confirm();
              break;
          }
        }
      );
    }
  );
}

void key_exchange::send_exchange()
{
  build_exchange();
  timer_start(rttTimeout_);
  asio::async_write(
    peer_,
    w_.view(0, w_.size()),
    SHARE_THIS(system::error_code ec, std::size_t nw)
    {
      LOG(INFO) << "exchange packet sent";
      timer_stop();
      if ( EC_CHK(ec)) return;
      w_.clear();
      switch ( type_ )
      {
        case type::SERVER:
          receive_confirm();
          break;
        case type::CLIENT:
          receive_exchange();
          break;
      }
    }
  );
}

bool key_exchange::validate_exchange()
{
  using namespace packet;
  auto exchange = (KeyExchange *) r_.view(sizeof(Header), r_.size()).data();
  std::string identify((char *) exchange->identify, IDENTIFY_LEN);
  auto iter = attr_.peerKeys.find(identify);
  if ( iter == attr_.peerKeys.end())
  {
    using namespace CryptoPP;
    std::string idB64;
    StringSource tSS0(identify, true,
                      new Base64Encoder(new StringSink(idB64), false));
    invokeError(ERRNOPEER, "No such user: " + idB64);
    return true;
  }
  bool validSign =
    util::ECDSA().verify(
      buffer(exchange->signature),
      buffer((KeyExchangeSign *) (exchange), sizeof(KeyExchangeSign)),
      iter->second
    );
  if ( !validSign )
  {
    using namespace CryptoPP;
    std::string signB64;
    ArraySource tAS0(
      exchange->signature, SIGNATURE_LEN,
      true, new Base64Encoder(new StringSink(signB64)));
    invokeError(ERRINVSIG, "Invalid signature: " + signB64);
    return true;
  }
  std::string key(32, '\0');
  util::ECDH::Public otherPub((char *) exchange->publicKey, PUBLIC_KEY_LEN);
  bool goodAgree = util::ECDH().agree(
    buffer(key),
    otherPub, lECDHPri_
  );
  if ( !goodAgree )
  {
    using namespace CryptoPP;
    invokeError(ERRFAILAGREE, "Failed to agree a key");
    return true;
  }
  pAttr_.identify = identify;
  pAttr_.publicKey = iter->second;
  pAttr_.key = key;
  return false;
}

void key_exchange::send_confirm()
{
  using namespace packet;
  w_.clear();
  {
    auto header = (Header *) w_.prepare(sizeof(Header)).data();
    header->type = native_to_big(packet_type(TYP_KEYCONFIRM));
    header->length = native_to_big(length_type(sizeof(KeyConfirm)));
    w_.commit(sizeof(Header));
  }
  {
    auto confirm = (KeyConfirm *) w_.prepare(sizeof(KeyConfirm)).data();
    util::sha256(buffer(pAttr_.key), buffer(confirm->digest));
    w_.commit(sizeof(KeyConfirm));
  }
  timer_start(rttTimeout_);
  asio::async_write(
    peer_, w_.view(0, w_.size()),
    SHARE_THIS(system::error_code ec, std::size_t nw)
    {
      timer_stop();
      if ( EC_CHK(ec)) return;
      invokeSuccess(std::move(pAttr_));
    }
  );
}

void key_exchange::receive_confirm()
{
  using namespace packet;
  r_.clear();
  timer_start(rttTimeout_);
  auto headerBf = r_.prepare(sizeof(Header));
  asio::async_read(
    peer_,headerBf,
    SHARE_THIS(system::error_code ec,std::size_t nr)
    {
      timer_stop();
      if ( EC_CHK(ec)) return;
      r_.commit(nr);
      auto header = util::make<Header>();
      r_.view(header);
      header.type = big_to_native(header.type);
      header.length = big_to_native(header.length);
      if ( header.type != TYP_KEYCONFIRM )
      {
        invokeError(ERRINVPKT, fmt::format("invalid type: {}", header.type));
        return;
      }
      if ( header.length != sizeof(KeyConfirm))
      {
        invokeError(ERRINVPKT,
                    fmt::format("invalid length: {}", header.length));
      }
      auto confirmBf = r_.prepare(sizeof(KeyConfirm));
      timer_start(rttTimeout_);
      asio::async_read(
        peer_, confirmBf,
        SHARE_THIS(system::error_code ec, std::size_t nr)
        {
          timer_stop();
          if ( EC_CHK(ec)) return;
          r_.commit(nr);
          auto confirm = (KeyConfirm *) r_.view(sizeof(Header), r_.size())
                                          .data();
          std::array<uint8_t, 32> local_digest;
          util::sha256(buffer(pAttr_.key), buffer(local_digest));
          if ( memcmp(local_digest.data(), confirm->digest, 32))
          {
            invokeError(ERRPEERINVKEY, "peer send a invalid key digest");
            return;
          }
          invokeSuccess(std::move(pAttr_));
        }
      );
    }
  );
}

}

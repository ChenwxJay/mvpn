//
// Created by maxtorm on 18-9-14.
//

#ifndef MVPN_DEVICE_TUN_H
#define MVPN_DEVICE_TUN_H

#include "detail/basic_tun.hpp"
#include "detail/basic_tun_service.hpp"

namespace mvpn::device
{
using tun = detail::basic_tun<detail::basic_tun_service>;
}

#endif //MVPN_DEVICE_TUN_H

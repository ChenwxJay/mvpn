//
// Created by maxtorm on 18-10-23.
//

#ifndef MVPN_DEVICE_CONFIG_HPP
#define MVPN_DEVICE_CONFIG_HPP

#include <string>
#include <boost/property_tree/ptree.hpp>
namespace mvpn::device
{

struct config
{
  std::string device;
  std::string ip;
  std::string prefix;
  int mtu = 1500;
  config() = default;
  explicit config(const boost::property_tree::ptree &interface)
  {
    from_property_tree(interface);
  }
  
  void from_property_tree(const boost::property_tree::ptree &iface)
  {
    mtu = iface.get<int>("MTU");
    device = iface.get<std::string>("Device");
    auto addr = iface.get<std::string>("Address");
    auto pos = addr.find('/');
    ip = addr.substr(0,pos);
    prefix = addr.substr(pos+1);
  }
};

}
#endif //MVPN_DEVICE_CONFIG_HPP

//
// Created by maxtorm on 18-9-15.
//

#ifndef MVPN_DEVICE_DETAIL_BASIC_TUN_H
#define MVPN_DEVICE_DETAIL_BASIC_TUN_H

#include <boost/asio/basic_io_object.hpp>
#include <boost/asio/ip/address.hpp>
#include <string>
#include <device/config.hpp>

namespace mvpn::device::detail
{
template<typename Service>
class basic_tun :
  public boost::asio::basic_io_object<Service>,
  public boost::noncopyable
{
public:
  explicit basic_tun(boost::asio::io_context &context) :
    boost::asio::basic_io_object<Service>(context)
  {};
  
  void open(const mvpn::device::config &config)
  {
    this->get_service().open(this->get_implementation(), config);
  }

  void close()
  {
    this->get_service().close(this->get_implementation());
  }
  
  template<typename MutableBufferSequence, typename ReadHandler>
  BOOST_ASIO_INITFN_RESULT_TYPE
  (ReadHandler, void (boost::system::error_code, std::size_t))
  async_read_some
    (const MutableBufferSequence &buffers,
     BOOST_ASIO_MOVE_ARG(ReadHandler)handler)
  {
    return this->get_service().async_read_some
      (this->get_implementation(), buffers,
       std::forward<ReadHandler>(handler));
  }

  template
    <
      typename ConstBufferSequence,
      typename WriteHandler
    >
  BOOST_ASIO_INITFN_RESULT_TYPE
  (
    WriteHandler,
    void (boost::system::error_code, std::size_t)
  )
  async_write_some
    (
      const ConstBufferSequence &buffers,
      BOOST_ASIO_MOVE_ARG(WriteHandler)handler
    )
  {
    return this->get_service().async_write_some
      (
        this->get_implementation(),
        buffers,
        std::forward<WriteHandler>(handler)
      );
  }
};
}

#endif

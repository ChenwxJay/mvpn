#ifndef MVPN_DEVICE_DETAIL_BASIC_TUN_SERVICE_H
#define MVPN_DEVICE_DETAIL_BASIC_TUN_SERVICE_H

#include <memory>
#include <boost/asio/io_context.hpp>
#include <device/detail/tun_impl_linux.hpp>

namespace mvpn::device::detail
{

class basic_tun_service : public boost::asio::io_context::service,
                          public boost::noncopyable
{
public:
  using key_type = basic_tun_service;
  using internal_impl_t = tun_impl_linux;
  using implementation_type = std::unique_ptr<internal_impl_t>;
  static boost::asio::io_context::id id ;
  
  explicit basic_tun_service(boost::asio::io_context &owner) :
    boost::asio::io_context::service(owner)
  {};
  
  ~basic_tun_service() override = default;
  
  implementation_type null() const
  {
    return nullptr;
  }
  
  void destroy(implementation_type &impl)
  {
    impl.reset(nullptr);
  }
  
  void construct(implementation_type &impl)
  {
    impl = std::make_unique<internal_impl_t>(get_io_context());
  }
  
  void open(implementation_type &impl, const mvpn::device::config &config)
  {
    impl->open(config);
  }
  
  void close(implementation_type &impl) const
  {
    impl->close();
  }
  
  template
    <
      typename MutableBufferSequence,
      typename ReadHandler
    >
  BOOST_ASIO_INITFN_RESULT_TYPE
  (
    ReadHandler,
    void (boost::system::error_code, std::size_t)
  )
  async_read_some
    (
      implementation_type &impl,
      const MutableBufferSequence &buffers,
      BOOST_ASIO_MOVE_ARG(ReadHandler)handler
    )
  {
    return impl->async_read_some(buffers, std::forward<ReadHandler>(handler));
  }
  
  template
    <
      typename ConstBufferSequence,
      typename WriteHandler
    >
  BOOST_ASIO_INITFN_RESULT_TYPE
  (
    WriteHandler,
    void (boost::system::error_code, std::size_t)
  )
  async_write_some
    (
      implementation_type &impl,
      const ConstBufferSequence &buffers,
      BOOST_ASIO_MOVE_ARG(WriteHandler)handler
    )
  {
    return impl->async_write_some(buffers, std::forward<WriteHandler>(handler));
  }

private:
  
  void shutdown() override
  {
  }
  
};

}
#endif

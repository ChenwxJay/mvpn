//
// Created by maxtorm on 2018/9/18.
//
# if 0
#ifndef MVPN_TUN_IMPL_WIN_H
#define MVPN_TUN_IMPL_WIN_H
#ifdef MVPN_PLATFORM_WIN
#include <tchar.h>
#include <windows.h>
#include <winioctl.h>
#include <ini/config.h>
#include <tap/windows.h>
#include <boost/asio/windows/random_access_handle.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <fmt/core.h>
#include "utility.hpp"

namespace mvpn::detail
{

class tun_impl_win : public boost::noncopyable
{
private:
  HANDLE tun_handle_;
  boost::asio::windows::random_access_handle random_access_handle_;
public:
  explicit tun_impl_win(boost::asio::io_context &ctx) :
    tun_handle_(INVALID_HANDLE_VALUE),
    random_access_handle_(ctx)
  {}

  ~tun_impl_win()
  {
    close();
  }

  HANDLE native_handler()
  {
    return tun_handle_;
  }

  void open(const mini::config &cfg)
  {
    std::string dev_guid;
    std::string dev_name = cfg.get_option("LOCAL","Interface");
    search_device(dev_name, dev_guid);
    set_mtu(dev_guid, cfg.get_option("LOCAL", "MTU"));
    netsh_device_restart(dev_name);
    auto tun_handle = create_handle(dev_guid);

    auto address = cfg.get_option("LOCAL", "InterfaceAddress");
    auto mask = cfg.get_option("LOCAL", "InterfaceMask");
    auto gateway = cfg.get_option("LOCAL", "InterfaceGateway");

    set_tun(tun_handle, address, mask);
    netsh_ip_address(dev_name,address,mask,gateway);
    turn_up(tun_handle);
    tun_handle_ = tun_handle;
    random_access_handle_.assign(tun_handle);
  }

  void close()
  {
    if (tun_handle_ != INVALID_HANDLE_VALUE)
      ::CloseHandle(tun_handle_);
  }

  template
    <
      typename MutableBufferSequence,
      typename ReadHandler
    >
  BOOST_ASIO_INITFN_RESULT_TYPE
  (
    ReadHandler,
    void (boost::system::error_code, std::size_t)
  )
  async_read_some
    (
      const MutableBufferSequence &buffers,
      BOOST_ASIO_MOVE_ARG(ReadHandler)handler
    )
  {
    return random_access_handle_.async_read_some_at(0, buffers,
                                                    std::forward<BOOST_ASIO_MOVE_ARG(ReadHandler)>(handler));
  }


  template
    <
      typename ConstBufferSequence,
      typename WriteHandler
    >
  BOOST_ASIO_INITFN_RESULT_TYPE
  (
    WriteHandler,
    void (boost::system::error_code, std::size_t)
  )
  async_write_some
    (
      const ConstBufferSequence &buffers,
      BOOST_ASIO_MOVE_ARG(WriteHandler)handler
    )
  {
    return random_access_handle_.async_write_some_at(0, buffers,
                                                     std::forward<BOOST_ASIO_MOVE_ARG(WriteHandler)>(handler));
  }

private:
  // WARNING: ANY HANDLE WILL BE CLOSE
  // NETWORK WILL RESTART
  // BE SURE CALL IT BEFORE CALLING CREATEFILE
  void netsh_device_restart(std::string_view dev_name)
  {
    const auto fmt_str = "netsh interface set interface \"{}\" {}";
    auto stop_cmd = fmt::format(fmt_str, dev_name, "disable");
    auto start_cmd = fmt::format(fmt_str, dev_name, "enable");
    errno = 0;
    system(stop_cmd.c_str());
    util::unix_error_check(errno,"netsh disable");
    system(start_cmd.c_str());
    util::unix_error_check(errno,"netsh enable");
  }

  void netsh_ip_address(std::string_view dev_name, std::string_view address, std::string_view mask,
                        std::string_view gateway)
  {

    auto address_cmd = fmt::format("netsh interface ip set address \"{}\" static {} {} {}", dev_name, address, mask,
                                   gateway);
    errno = 0;
    system(address_cmd.c_str());
    util::unix_error_check(errno,"netsh ip set address");

  }

  void set_mtu(std::string_view dev_guid,std::string_view mtu)
  {
    HKEY adapter_index_key;
    auto err_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE, ADAPTER_KEY, 0, KEY_READ, &adapter_index_key);
    util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegOpenKeyEx(adapter_index_key)");
    auto adapter_index_key_guarder = util::make_hkey_guard(adapter_index_key);
    for (DWORD index = 0;; index++)
    {
      TCHAR device_index_as_path[256];
      DWORD device_index_len = sizeof(device_index_as_path);
      err_code = RegEnumKeyEx(
        adapter_index_key,
        index,
        device_index_as_path,
        &device_index_len,
        nullptr, nullptr, nullptr, nullptr);
      util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegEnumKeyEx(adapter_index_key)");

      HKEY adapter_key;
      auto adapter_path = fmt::format("{}\\{}", ADAPTER_KEY, device_index_as_path);
      err_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE, adapter_path.c_str(), 0, KEY_ALL_ACCESS, &adapter_key);

      util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegOpenKeyEx(adapter_key)");
      auto adapter_key_guarder = util::make_hkey_guard(adapter_key);
      TCHAR NetCfgInstanceId_value[256];
      DWORD NetCfgInstanceId_len = sizeof(NetCfgInstanceId_value);
      DWORD NetCfgInstanceId_type;//NOT USED
      err_code = RegQueryValueExA(
        adapter_key,
        TEXT("NetCfgInstanceId"),
        nullptr,
        &NetCfgInstanceId_type,
        (LPBYTE) NetCfgInstanceId_value, &NetCfgInstanceId_len);
      if (err_code != ERROR_FILE_NOT_FOUND)
      {
        util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegQueryValueEx(NegCfgInstaceId)");
        if (dev_guid == NetCfgInstanceId_value)
        {
          err_code = RegSetValueEx(adapter_key, TEXT("MTU"), 0, REG_SZ, (const BYTE *)mtu.data(),
                                   static_cast<DWORD>(mtu.size()));
          util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegSetValueEx(MTU)");
          return;
        }
      }
    }
  }

  HANDLE create_handle(std::string_view dev_guid)
  {
    auto tap_filename = fmt::format("{}{}{}", USERMODEDEVICEDIR, dev_guid, TAP_WIN_SUFFIX);
    HANDLE tun_handle = CreateFile(tap_filename.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING,
                                   FILE_FLAG_OVERLAPPED | FILE_ATTRIBUTE_SYSTEM, nullptr);
    if (tun_handle == INVALID_HANDLE_VALUE)
    {
      throw mvpn_error(util::windows_error_format(::GetLastError(),"CreateFile()"));
    }
    return tun_handle;
  }

  void set_tun(HANDLE handle, std::string_view address, std::string_view mask)
  {
    ULONG ep[3];
    ep[0] = inet_addr(address.data());
    ep[2] = inet_addr(mask.data());
    ep[1] = ep[0] & ep[2];
    DWORD ep_len = sizeof(ep);
    DeviceIoControl(
      handle,
      TAP_WIN_IOCTL_CONFIG_TUN,
      ep, ep_len,
      ep, ep_len, &ep_len,
      nullptr
    );
    util::windows_check_expect(ERROR_SUCCESS, (decltype(ERROR_SUCCESS)) GetLastError(),
                               "DeviceIoControl(TAP_WIN_IOCTL_CONFIG_TUN)");
  }

  void turn_up(HANDLE handle)
  {
    ULONG status = TRUE;
    DWORD status_len = sizeof(status);
    DeviceIoControl(
      handle,
      TAP_WIN_IOCTL_SET_MEDIA_STATUS,
      &status, status_len,
      &status, status_len,
      &status_len,
      nullptr);
    util::windows_check_expect(ERROR_SUCCESS, (decltype(ERROR_SUCCESS)) GetLastError(),
                               "DeviceIoControl(TAP_WIN_IOCTL_SET_MEDIA_STATUS)");
  }

  void search_device(std::string_view dev_name, std::string &dev_guid)
  {
    HKEY network_key;
    auto err_code = RegOpenKeyEx(HKEY_LOCAL_MACHINE, NETWORK_CONNECTIONS_KEY, 0, KEY_READ, &network_key);
    util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegOpenKeyEx(network_key)");
    auto network_key_guarder = util::make_hkey_guard(network_key);
    bool find_device = false;

    // enum each connection
    for (DWORD iter = 0; !find_device; iter++)
    {
      TCHAR device_guid_as_path[256];
      DWORD device_guid_len = sizeof(device_guid_as_path);

      err_code = RegEnumKeyEx(
        network_key,
        iter,
        device_guid_as_path,
        &device_guid_len,
        nullptr, nullptr, nullptr, nullptr);
      util::windows_check_unexpect(ERROR_NO_MORE_ITEMS, static_cast<DWORD>(err_code), "RegEnumKeyEx");
      util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegEnumKeyEx");
      // open Network/${device_guid_as_path}/Connection
      HKEY connection_key;
      auto connection_path = fmt::format("{}\\{}\\Connection",NETWORK_CONNECTIONS_KEY,device_guid_as_path);
      err_code = RegOpenKeyEx(
        HKEY_LOCAL_MACHINE,
        connection_path.c_str(),
        0,
        KEY_READ,
        &connection_key
      );

      if (err_code == ERROR_FILE_NOT_FOUND)
        continue;
      util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegOpenKeyEx(connection_key)");

      auto connection_key_guarder = util::make_hkey_guard(connection_key);
      TCHAR device_name[256];
      DWORD device_name_type;// Not used
      DWORD device_name_len = sizeof(device_name);
      //QueryValue name
      err_code = RegQueryValueEx(
        connection_key,
        TEXT("Name"),
        nullptr,
        &device_name_type,
        (LPBYTE) device_name,
        &device_name_len
      );
      util::windows_check_expect(ERROR_SUCCESS, static_cast<DWORD>(err_code), "RegQueryValueEx(Name)");

      if (dev_name == device_name)
      {
        find_device = true;
        dev_guid.assign(device_guid_as_path, device_guid_len);
      }
    }

  }

};

}
#endif //MVPN_PLATFORM_WIN
#endif //MVPN_TUN_IMPL_WIN_H
#endif

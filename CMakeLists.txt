cmake_minimum_required(VERSION 3.12)
project(mvpn)
add_definitions(-DBOOST_ASIO_NO_DEPRECATED)
add_definitions(-DBOOST_COROUTINES_NO_DEPRECATION_WARNING)
if (WIN32)
    add_definitions(-DMVPN_PLATFORM_WIN)
elseif (CMAKE_SYSTEM_NAME STREQUAL "Linux")
    add_definitions(-DMVPN_PLATFORM_LINUX)
else ()
    MESSAGE(FATAL_ERROR "Unknow platform :${CMAKE_SYSTEM_NAME}")
endif ()

set(CMAKE_CXX_STANDARD 17)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin)
set(LIBRARY_OUTPUT_PATH  ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_CXX_FLAGS_DEBUG -g )
set(CMAKE_CXX_FLAGS "-Wall -Werror -Wextra -Wno-unused-parameter -Wno-unknown-pragmas")
set(CMAKE_CXX_FLAGS_RELEASE -O3 )

add_subdirectory(third_party/zlib)
add_subdirectory(third_party/fmt)
add_subdirectory(src)

